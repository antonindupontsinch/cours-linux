#!/usr/bin/env bash
set -euo pipefail

dir_to_backup='/var/www/'
destination='/opt/backup'

backup () {
    mkdir -p "${destination}"
    rsync -rauL "${dir_to_backup}" "${destination}/$(date -u +'%Y-%m-%d')"
}

rotate () {
    for backup in $(find ${destination} -maxdepth 1 -type d | sort --reverse | tail --lines=+8); do
        rm -rf "${backup}"
    done
}

backup
rotate
